import gym
env = gym.make('BipedalWalker-v2')
for i_episode in range(20):
    observation = env.reset()
    for t in range(100):
        env.render()

        # Sample: 24x1
        # [ 1.10276008e+00  1.46366758e-01 -7.97149730e-02 -6.67166042e-02
        #  -4.84871380e-02 -8.93628597e-01 -6.09373808e-01  1.66107019e-02
        #   1.00000000e+00 -8.63286704e-02 -5.30823112e-01 -6.31046057e-01
        #  -2.78155009e-07  0.00000000e+00  3.24573517e-01  3.28259528e-01
        #   3.39747846e-01  3.60458195e-01  3.93262476e-01  4.43595022e-01
        #   5.22149801e-01  6.52315974e-01  8.95726740e-01  1.00000000e+00 ]
        #
        # Explanation
        #   Size: 24x1
        #   Items:
        #       1 - Hull angle (angulo del casco)
        #       2 - Hull angular velocity (velocidad angular del casco)
        #       3 - Velocidad en X
        #       4 - Velocidad en Y
        #       5 - Pie 0: Juntura 0: angulo
        #       6 - Pie 0: Juntura 0: velocidad
        #       7 - Pie 0: Juntura 1: angulo
        #       8 - Pie 0: Juntura 1: velocidad
        #       9 - Pie 0: Contacto con el suelo (bool)
        #       10- Pie 1: Juntura 2: angulo
        #       11- Pie 1: Juntura 2: velocidad
        #       12- Pie 1: Juntura 3: angulo
        #       13- Pie 1: Juntura 3: velocidad
        #       14- Pie 1: Contacto con el suelo (bool)
        #       15~24- 10 Lidar rangefingers
        #
        print(observation)

        action = env.action_space.sample()

        print("--------- ACTION ---------")

        # Sample: action = [-0.8856344   0.24836256  0.8695198  -0.20760578]
        #
        # Explanation:
        #   Each _action item_ is related with a joint (juntura) motor speed
        #   and motor torque:
        #       action[0] is related with joints[0]
        #       action[1] is related with joints[1]
        #       action[2] is related with joints[2]
        #       action[3] is related with joints[3]
        #
        # See
        #   -https://github.com/openai/gym/blob/master/gym/envs/box2d/bipedal_walker.py
        # (377 ~ 390)
        print(action)

        print("--------------------------")

        observation, reward, done, info = env.step(action)

        print("*** reward: {:+f}".format(reward))

        if done:
            print("Episode finished after {} timesteps".format(t + 1))
            break
