import matplotlib.pyplot as plt
import numpy as np
import matplotlib.mlab as mlab

from select_methods_nn import sus

from drawing import histogram_selection

rewards_arr = np.array([
	 -128.88924651,
	 -108.79441587,
	 -118.20217241,
	 -120.81624648,
	 -100.66532633,
	 -125.16232583,
	  10.93293302,
	 -6.83121611,
	 6.82507874,
	 16.60459299,
	 -76.33055875,
	 -56.16965236,  
	 -16.16261418,
	 16.0751678,
	  15.48091001,
	 -15.01831158,
	 -44.93942315,
	 -14.69625894,
	 -14.66050632,
	 -14.39247127])

select_nitems = 16

# Selecting population to cross over and mutation
selected_idxs = sus(rewards_arr, select_nitems)

# histogram of selection

# histogram_selection(selected_idxs)

# x = columns
# x = columns[0]
x = np.sort(selected_idxs, axis=None)
# num_bins = columns.shape[0]
num_bins = rewards_arr.shape[0]

x_count = np.bincount(x.astype(int))
x_rango = np.arange(x_count.shape[0])

# x_count = np.sort(x_count)
print("x : {}".format(x))
print("x _rango: {}".format(x_rango))
print("x _count: {}".format(x_count))

plt.bar(x_rango,x_count)
plt.show()

# example data
# mu = 100 # mean of distribution
# sigma = 15 # standard deviation of distribution
# x = mu + sigma * np.random.randn(10000)

# print 

print("num_bins : {}".format(num_bins))

# the histogram of the data
n, bins, patches = plt.hist(x, num_bins, density=False, facecolor='blue', alpha=0.5)


# add a 'best fit' line
# y = mlab.normpdf(bins, mu, sigma)
# plt.plot(bins, y, "r--")
plt.xlabel("idxs")
plt.ylabel("Frequency")
plt.title(r"histogram Population Selected")
 
# Tweak spacing to prevent clipping of ylabel
plt.subplots_adjust(left=0.15)
plt.show()
