import numpy as np
#
#
#


def logsig(x):
    """
        Function logsig
        Returned values are within [0,1]
    """
    logsig_value = 1 / (1 + np.exp(-x))
    return logsig_value


def tansig(x):
    """
        Function tansig
        Returned values are within [-1,1]
    """
    tansig_value = 1 / (1 + np.exp(-2 * x)) - 1
    return tansig_value
