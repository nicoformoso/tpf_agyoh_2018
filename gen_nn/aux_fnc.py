# Some useful functions
import numpy as np


def save_nparray(array_to_save, csv_file, delim=","):
    """
        Saves a numpy array as csv
    """
    fmt = "%.6f"
    np.savetxt(csv_file, array_to_save, fmt=fmt, delimiter=delim)
