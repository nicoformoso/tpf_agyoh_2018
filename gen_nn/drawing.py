import matplotlib.pyplot as plt
import numpy as np


def draw_rew_vs_gen(columns):
    x_axis = np.arange(columns.shape[0])

    for y_idx in xrange(0, columns.shape[1]):
        y_axis = columns[:, y_idx]

        # This is nasty, I know
        if y_idx == 0:
            label = "Best rewards"
        else:
            label = "Avg. rewards"

        plt.plot(x_axis, y_axis, label=label)

    plt.title("Rewards vs. Generations")
    plt.xlabel("Generations")
    plt.ylabel("Rewards")
    plt.legend()
    plt.show()
