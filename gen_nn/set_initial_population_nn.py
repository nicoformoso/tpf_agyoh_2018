import random
import numpy as np


def initial_population(num_rows, num_cols, num_matrix=1):
    """
        This function returns a numpy array of num_rows by num_cols size,
        whose values are random between -1 and 1.
    """

    min_val = -3
    max_val = 3

    solution = np.empty([num_rows, num_cols])
    population = np.empty([num_matrix, num_rows, num_cols])

    for idx_matrix in xrange(0, num_matrix):

        for idx_row in xrange(0, num_rows):
            for idx_col in xrange(0, num_cols):
                solution[idx_row, idx_col] = random.uniform(min_val, max_val)
        population[idx_matrix, :, :] = solution

    if num_matrix == 1:
        return solution

    else:
        return population
