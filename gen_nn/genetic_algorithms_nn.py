import numpy as np
from eval_function import eval_function
from select_methods_nn import sus

from mutations_nn import mutation_non_uniform as do_mutation
from crossovers_nn import cube_xover as do_xover

from drawing import draw_rew_vs_gen


def single_genetic(n_generations, init_population, selection_percent,
                   xover_percent, mutate_percent, elite_nitems):
    """
    Function single_genetic
    Executes a genetic algorithm over a 2D array.

    Input variables
      - n_generations  : Integer. Number of generations
      - init_population: 2D Numpy Array. Each row of this is a solution
      - has_xover      : Bool. Defines if we have to apply Crossover
      - xover_percent  : Float. Amount percent of solutions which will be
                         crossed over
      - has_mutate     : Bool. Defines if Mutation is applied
      - mutate_percent : Float. Amount percent of solutions which will be
                         mutated
      - has_elite      : Bool. Defines if Elite is applied
      - elite_nitems   : Integer. Number of items which will be considered
                         Elite

    Output variables
      - population      : 2D Numpy Array. The best solution.
    """
    population = init_population

    # computing sizes
    [num_actions, num_items_per_action] = population.shape

    select_nitems = np.ceil(
        selection_percent * (num_actions - elite_nitems)).astype(int)

    # This numpy array is used to graph results.
    # Description:
    #  - index of array == no generation
    #  - first element  == best reward in generation
    #  - second element == average reward in generation
    rew_by_gen = np.empty([n_generations, 2])

    print("Selected Individuals: {}".format(select_nitems))

    print("\n*** GENERATION 0 ***")
    # Evaluating initial population
    rewards_arr = eval_function(init_population)

    # Saving results
    best_reward = rewards_arr[np.argmax(rewards_arr)]
    avge_reward = np.mean(rewards_arr)
    rew_by_gen[0, :] = [best_reward, avge_reward]

    # Screen message
    print("- Best reward: {:+f}\n".format(best_reward))

    # Running through generations
    for generation in xrange(1, n_generations):

        print("*** GENERATION {:d} ***".format(generation))

        # Elite
        elite_items = np.zeros([elite_nitems, num_items_per_action])
        elite_rewards = np.zeros([elite_nitems])

        print("- Elite: ")
        for elite_count in xrange(0, elite_nitems):
            elite_item_idx = np.argmax(rewards_arr)

            # filling arrays
            elite_items[elite_count, :] = population[elite_item_idx, :]
            elite_rewards[elite_count] = rewards_arr[elite_item_idx]

            # removing from originals
            population = np.delete(population, elite_item_idx, 0)
            rewards_arr = np.delete(rewards_arr, elite_item_idx, 0)

        # Inserting elite again.
        population = np.append(population, elite_items, axis=0)
        rewards_arr = np.append(rewards_arr, elite_rewards, axis=0)

        print("-- Elite individuals taken: {}".format(elite_rewards))

        print("- Selection: ")
        # Selecting population to cross over and mutation
        selected_idxs = sus(rewards_arr, select_nitems)

        real_selected = selected_idxs.size

        selected_items = population[selected_idxs.astype(int), :]
        selected_rewards = rewards_arr[selected_idxs.astype(int)]

        print("- Total selected individuals: {}".format(real_selected))

        # Taking out selected items from main arrays
        population = np.delete(population, selected_idxs, 0)
        rewards_arr = np.delete(rewards_arr, selected_idxs, 0)

        # Shuffle selected items
        # np.random.shuffle(selected_items)

        all_selected_items = selected_items
        all_selected_rew = selected_rewards

        # Crossover
        xover_nitems = np.ceil(xover_percent * real_selected).astype(int)

        # Number of items for crossover shouldn't be odd
        if np.mod(xover_nitems, 2):
            xover_nitems -= 1

        print("Individuals Crossed-Over: {}".format(xover_nitems))

        xover_items = np.zeros([xover_nitems, num_items_per_action])
        xover_rewards = np.zeros([xover_nitems])

        for xover_idx in xrange(0, (xover_nitems - 1), 2):
            fst_elem = selected_items[xover_idx, :]
            snd_elem = selected_items[(xover_idx + 1), :]

            [fst_elem_cross, snd_elem_cross] = do_xover(fst_elem, snd_elem)

            xover_items[xover_idx, :] = fst_elem_cross
            xover_items[(xover_idx + 1), :] = snd_elem_cross
            xover_rewards[xover_idx] = None  # Every new action ...
            xover_rewards[xover_idx + 1] = None  # ... isn't evaluated yet

        selected_items = np.delete(selected_items, np.s_[0, xover_nitems], 0)
        selected_rewards = np.delete(selected_rewards, np.s_[0, xover_nitems])

        # Mutation
        mutate_nitems = np.ceil(mutate_percent * real_selected).astype(int)

        if real_selected < (xover_nitems + mutate_nitems):
            mutate_nitems = real_selected - xover_nitems

        print("Individuals mutated: {}".format(mutate_nitems))

        mutated_items = np.zeros([mutate_nitems, num_items_per_action])
        mutated_rewards = np.zeros([mutate_nitems])

        for mutate_idx in xrange(0, mutate_nitems):
            mutated = do_mutation(
                selected_items[mutate_idx, :], generation, n_generations)

            mutated_items[mutate_idx, :] = mutated
            mutated_rewards[mutate_idx] = None

        old_population = population
        old_rewards_arr = rewards_arr

        # Merging/Concatenating/Appending everything
        population = np.concatenate(
            (elite_items, xover_items, mutated_items), axis=0)

        rewards_arr = np.concatenate(
            (elite_rewards, xover_rewards, mutated_rewards), axis=0)

        # Adding original selected elements
        while population.shape[0] < num_actions:
            if all_selected_items.shape[0] > 0:
                item_rnd_sel = np.random.randint(
                    0, all_selected_items.shape[0])

                population = np.append(
                    population,
                    np.reshape(all_selected_items[item_rnd_sel, :], (1, -1)),
                    axis=0)

                rewards_arr = np.append(
                    rewards_arr,
                    np.reshape(all_selected_rew[item_rnd_sel], (-1)),
                    axis=0)

                all_selected_items = np.delete(
                    all_selected_items, item_rnd_sel, 0)
                all_selected_rew = np.delete(all_selected_rew, item_rnd_sel, 0)
            else:
                break

        # Completing population size with old population (if needed)
        while population.shape[0] < num_actions:
            if old_population.shape[0] > 0:
                item_rnd_sel = np.random.randint(0, old_population.shape[0])

                population = np.append(
                    population,
                    np.reshape(old_population[item_rnd_sel, :], (1, -1)),
                    axis=0)

                rewards_arr = np.append(
                    rewards_arr,
                    np.reshape(old_rewards_arr[item_rnd_sel], (-1)),
                    axis=0)

                old_population = np.delete(old_population, item_rnd_sel, 0)
                old_rewards_arr = np.delete(old_rewards_arr, item_rnd_sel, 0)
            else:
                break

        # Evaluating new population
        rewards_arr = eval_function(population)

        # Saving results
        best_reward = rewards_arr[np.argmax(rewards_arr)]
        avge_reward = np.mean(rewards_arr)
        rew_by_gen[generation, :] = [best_reward, avge_reward]

        # Screen message
        print("- Best reward: {:+f}\n".format(best_reward))

    # End 'for' n_generations

    # Drawing
    draw_rew_vs_gen(rew_by_gen)

    return rewards_arr, population
