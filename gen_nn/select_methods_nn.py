import numpy as np
import random
from probabilities_nn import calc_probabilities


def ruleta(values, shoots):
    """
        This is the common roulette algorithm.
        It computes the probability of each element into 'values',
        and then selects a number of those elements, given by 'shoots'
    """
    probabilities = calc_probabilities(values)
    acc_probab = np.cumsum(np.divide(probabilities, np.sum(probabilities)))

    prob_array = (np.arange(0, shoots, dtype=np.float64) /
                  shoots) + (random.random() / shoots)
    selected_values = np.zeros(shoots)

    prob_idx = 0
    shoots_idx = 0

    while shoots_idx < shoots:
        if prob_array[shoots_idx] < acc_probab[prob_idx]:
            selected_values[shoots_idx] = prob_idx
            shoots_idx += 1
        else:
            prob_idx += 1

    return selected_values


def sus(values, shoots):
    """
        Stochastic Universal Sampling
    """
    total_fitness = np.abs(np.sum(values))
    dist_point = total_fitness / shoots
    start_point = np.random.uniform(0, dist_point)

    pointers = np.empty(shoots)
    for i in xrange(0, shoots):
        pointers[i] = start_point + (i * dist_point)

    selected_values = np.empty(0)

    for point in xrange(0, pointers.size):
        i = 0

        while np.abs(np.sum(values[0:i])) < pointers[point]:
            i += 1

        # if not np.isin(i, selected_values) and i < values.size:
        if i < values.size:
            selected_values = np.append(i, selected_values)

    # Selected values number should be even
    if np.mod(selected_values.size, 2):
        selected_values = np.delete(selected_values, 1, 0)

    return selected_values
