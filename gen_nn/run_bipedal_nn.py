import gym
import numpy as np

def run_bipedal( action_array ):
    """
        Function run_bipedal

        Input variables:
            - action_array   : 2D Numpy Array. A list of actions to be executed.
        Output variables:
            - result_rewards : 2D Numpy Array. Reward of each action
    """
    [num_actions, items_per_array] = action_array.shape
    result_rewards = np.empty([num_actions])

    env = gym.make('BipedalWalker-v2')
    env.reset()

    for idx_action in xrange(0,(num_actions-1)):
        action = action_array[idx_action,:]
        observation, reward, done, info = env.step(action)
        result_rewards[idx_action] = reward
        # print("-- reward: {:+f} --".format(reward))
        
        env.render()

        if done: break

    return result_rewards

