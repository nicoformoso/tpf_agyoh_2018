#
# This is the main executable script for running the genetic algorithm
# to reach the sequence of 'action' arrays to make the BipedalWalker-v2
# walk
#
import numpy as np
from set_initial_population_nn import initial_population as set_init_pop
from genetic_algorithms_nn import single_genetic as run_genetic
from eval_function import eval_function
from aux_fnc import save_nparray

#
# Variable definition
#

# Number of generations
num_generations = 30

# Number of solutions
num_solutions = 1

num_actions = 200
num_items_per_action = 452

# These variables below, define if crossover, mutation and elite
# operations should be done.

# Selection percent is computed over whole population size.
selection_percent = 0.70

# Crossover percent is computed over selection size.
xover_percent = 0.85

# Mutation percent is computed over selection without crossover.
mutate_percent = 0.15

# Elite is the number of best items within the whole new generation.
elite_nitems = 4

# Files
population_file = "./output/last_population.csv"
rewards_file = "./output/last_rewards.csv"

# Others
pop_to_test_count = 10

#
# Initial population generation
#
initial_rand_population = set_init_pop(
    num_actions, num_items_per_action, num_solutions)

print("\n*** Poblacion inicial aleatoria generada.\n")
print("*** Generando {:d} soluciones de {:d}x{:d}.\n".format(
    num_solutions, num_actions, num_items_per_action))

#
# Genetic Algorithm Execution
#
rewards, population = run_genetic(
    num_generations, initial_rand_population,
    selection_percent, xover_percent,
    mutate_percent, elite_nitems)

# Write results into file
save_nparray(population, population_file)
save_nparray(rewards, rewards_file)

# Running simulation
populations_to_test = np.empty([pop_to_test_count, num_items_per_action])

for pop_idx in xrange(0, pop_to_test_count):
    best_pop = np.argmax(rewards)
    populations_to_test[pop_idx, :] = population[best_pop, :]
    rewards = np.delete(rewards, best_pop, 0)

print("** Testing populations: {}".format(populations_to_test.shape))

eval_function(populations_to_test, True)
