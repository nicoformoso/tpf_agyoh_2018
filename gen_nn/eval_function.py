# Importing libs
import numpy as np
import gym
from transfer_fcn import tansig


def eval_function(population, render=False):
    # avg_count = 5
    # reward_avg = np.empty(avg_count)

    pop_size = population.shape[0]
    reward_arr = np.empty(pop_size)

    for pop_idx in xrange(0, pop_size):
        if render:
            print("Population no.: {}".format(pop_idx))

        # for x in xrange(0, avg_count):
            # reward_avg[x] = neural_network(population[pop_idx], render)

        reward_arr[pop_idx] = neural_network(population[pop_idx], render)
        # reward_arr[pop_idx] = np.mean(reward_avg)

    return reward_arr


def neural_network(weights, render=False):
    #
    # Bipedal walker definitions
    #

    # bipedal environment
    env = gym.make('BipedalWalker-v2')
    max_bipedal_steps = 150

    # initial values
    reward = 0
    reward_count = 0
    observation = env.reset()

    #
    # Neural network definitions
    #

    # layers

    # - input (n_input_vars) - layer 1
    input_vars_vec = np.empty([3])
    input_vars_vec[2] = 24
    input_vars_vec[1] = 29
    input_vars_vec[0] = 34
    # - hidden - layers 2 and 3
    # hidden_layer_neurons = 10
    hidden_layer2_neurons = 10
    hidden_layer3_neurons = 8
    # - output - layer 4
    n_output_vars = 4

    # This counter defines how many steps should be skipped before start
    # with the NN properly
    skip_steps_count = 2

    #
    # prev_* variables should be intended as follows
    # i.e.
    # prev_actions[0,:] == action vector from (t - 1)
    # prev_actions[1,:] == action vector from (t - 2)
    #
    # idem for prev_rewards
    #
    prev_actions = np.empty([skip_steps_count, n_output_vars])
    prev_rewards = np.empty([skip_steps_count, 1])

    while True:
        n_input_vars = input_vars_vec[skip_steps_count].astype(int)

        # number of weights
        theta1_num_params = hidden_layer2_neurons * n_input_vars
        theta2_num_params = hidden_layer3_neurons * hidden_layer2_neurons
        theta3_num_params = n_output_vars * hidden_layer3_neurons

        # matrixes of weights
        theta1 = np.empty([hidden_layer2_neurons, n_input_vars])
        theta2 = np.empty([hidden_layer2_neurons, hidden_layer3_neurons])
        theta3 = np.empty([n_output_vars, hidden_layer3_neurons])

        # shapping theta 1
        theta1_params = weights[0:theta1_num_params]
        theta1 = np.reshape(theta1_params,
                            (hidden_layer2_neurons, n_input_vars))

        # shapping theta 2
        theta2_params = weights[theta1_num_params:(
            theta1_num_params + theta2_num_params)]
        theta2 = np.reshape(theta2_params,
                            (hidden_layer3_neurons, hidden_layer2_neurons))

        # shapping theta 3
        theta3_params = weights[theta2_num_params:(
            theta2_num_params + theta3_num_params)]
        theta3 = np.reshape(theta3_params,
                            (n_output_vars, hidden_layer3_neurons))

        # End condition
        if not skip_steps_count:
            break

        previous_values = np.append(
            prev_actions[np.arange(2 - skip_steps_count), :].reshape(-1),
            prev_rewards[np.arange(2 - skip_steps_count)].reshape(-1))

        # setting input parameters
        input_x_params = np.append(observation, previous_values)

        # Computing layer activations
        layer_2 = np.apply_along_axis(
            tansig, 0, np.matmul(theta1, input_x_params))
        layer_3 = np.apply_along_axis(
            tansig, 0, np.matmul(theta2, layer_2))
        layer_4 = np.apply_along_axis(
            tansig, 0, np.matmul(theta3, layer_3))
        # Just to be clear:
        action = layer_4

        # Running step for bipedal walker
        observation, reward, done, info = env.step(action)

        prev_actions[(2 - skip_steps_count), :] = action
        prev_rewards[(2 - skip_steps_count)] = reward

        # last sentence into While-True
        skip_steps_count -= 1

    for step_idx in xrange(0, max_bipedal_steps):
        previous_values = np.append(
            prev_actions.reshape(-1), prev_rewards.reshape(-1))

        # setting input parameters
        input_x_params = np.append(observation, previous_values)

        # Computing layer activations
        layer_2 = np.apply_along_axis(
            tansig, 0, np.matmul(theta1, input_x_params))
        layer_3 = np.apply_along_axis(
            tansig, 0, np.matmul(theta2, layer_2))
        layer_4 = np.apply_along_axis(
            tansig, 0, np.matmul(theta3, layer_3))
        # Just to be clear:
        action = layer_4

        # Running a new step for bipedal walker
        observation, reward, done, info = env.step(action)

        prev_actions = np.delete(prev_actions, 1, 0)
        prev_actions = np.insert(prev_actions, 0, action, axis=0)
        prev_rewards = np.delete(prev_rewards, 1, 0)
        prev_rewards = np.insert(prev_rewards, 0, reward, axis=0)

        reward_count += reward

        if render:
            env.render()

        if done:
            env.close()
            break

    env.close()

    return reward_count
