import numpy as np
import random


def get_weights(num_weights):
    """
        Function get_weights
        This function returns num_weights random weights.
        Values are within [0,1]
        --
    """
    weights = gen_rnd_vector(num_weights, 0, 1)

    return weights


def gen_rnd_vector(max_num, range_min, range_max):
    """
        Function gen_rnd_vector
        ---
    """
    rnd_numbers = np.empty([max_num])

    for n_idx in xrange(0, max_num):
        rnd_numbers[n_idx] = random.uniform(range_min, range_max)

    return rnd_numbers
