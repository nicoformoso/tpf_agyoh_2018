import random
import numpy as np


def mutation(element):
    """
        Function mutation_single
        Changes ONLY ONE coordinate of the taken element. The new value
        is random within [0,1]
    """

    taken_element = random.randint(0, (element.size - 1))
    new_value = random.uniform(-3, 3)

    element[taken_element] = new_value

    return element


def mutation_non_uniform(element, actual_gen, total_gen):

    sigma = 3 * (1 - actual_gen / total_gen) + 0.5

    elements_to_mutate = np.ceil(
        (1 - (float(actual_gen) / float(total_gen))) * element.size).astype(int)

    if elements_to_mutate > 0.10 * element.size:
        elements_to_mutate = np.ceil(0.10 * element.size).astype(int)

    element_indexes = np.arange(0, element.size)
    np.random.shuffle(element_indexes)

    for element_idx in xrange(0, elements_to_mutate):
        index = element_indexes[element_idx]
        new_value = np.random.normal(element[index], sigma)

        element[index] = new_value

    return element
