import random
import numpy as np


def crossover(element1, element2):

    num_items = element1.size

    if (num_items != element2.size):
        print("ERROR :: crossover_single :: Input size doesn't match.")
        exit(1)

    # I should check if cut_point is integer
    cut_point = num_items / 2

    cross_element1 = element1
    cross_element1[cut_point:num_items] = element2[0:cut_point]
    cross_element2 = element2
    cross_element2[0:cut_point] = cross_element1[cut_point:num_items]

    return [cross_element1, cross_element2]


def crossover_random(element1, element2):

    size = min(len(element1), len(element2))

    cxpoint1 = random.randint(1, size)
    cxpoint2 = random.randint(1, size - 1)

    if cxpoint2 >= cxpoint1:
        cxpoint2 += 1
    else:
        cxpoint1, cxpoint2 = cxpoint2, cxpoint1

    aux = np.copy(element1)
    element1[cxpoint1:cxpoint2] = element2[cxpoint1:cxpoint2]
    element2[cxpoint1:cxpoint2] = aux[cxpoint1:cxpoint2]

    return element1, element2


def cube_xover(element1, element2):

    R = np.random.uniform(0, 1, element1.size)

    cube_xover_element1 = R * element1 + (1 - R) * element2
    cube_xover_element2 = (1 - R) * element1 + R * element2

    return cube_xover_element1, cube_xover_element2
