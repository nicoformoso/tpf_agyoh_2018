import numpy as np
import random
from probabilities import calc_probabilities

def single_ruleta( values, shoots ):
	"""
		This is the common roulette algorithm. 
		It computes the probability of each element into 'values',
		and then selects a number of those elements, given by 'shoots'
	"""
	probabilities = calc_probabilities( values );
	acc_probab	  = np.cumsum( np.divide(probabilities, np.sum( probabilities )) )

	prob_array 		= ( np.arange(0, shoots, dtype=np.float64)/shoots ) + ( random.random() / shoots )
	selected_values = np.zeros(shoots)

	prob_idx   = 0
	shoots_idx = 0

	while shoots_idx < shoots:
		if prob_array[shoots_idx] < acc_probab[prob_idx]:
			selected_values[shoots_idx] = prob_idx
			shoots_idx += 1
		else:
			prob_idx += 1

	return selected_values