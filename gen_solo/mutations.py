import numpy as np
import random

def mutation_single( element, seed = None ):
	"""
		Function mutation_single
		Changes ONLY ONE coordinate of the taken element. The new value
		is random within [-1,1]
	"""
	if seed:
		random.seed(seed)

	taken_element = random.randint(0, (element.size - 1))
	new_value 	  = random.uniform(-1,1)

	element[taken_element] = new_value

	return element

def mutation_multi( solution, seed = None ):
	"""
		Function mutation_multi
		Changes ONLY ONE coordinate of the taken solution. The new value
		is random within [-1,1]
	"""
	if seed:
		random.seed(seed)

	taken_action = random.randint(0, solution.shape[0] )

	new_item_value = np.array([
		random.uniform(-1,1),
		random.uniform(-1,1),
		random.uniform(-1,1),
		random.uniform(-1,1)])

	solution[taken_action] = new_item_value

	return solution