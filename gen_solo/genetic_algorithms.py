import numpy as np

# from run_bipedal import run_bipedal_single as run_bipedal
from run_bipedal import run_bipedal_multi as run_bipedal

from select_methods import single_ruleta as ruleta

# from crossovers import crossover_single as do_xover
from crossovers import crossover_multi as do_xover

# from mutations import mutation_single as do_mutation
from mutations import mutation_multi as do_mutation

def single_genetic_single_sol(n_generations, init_population, selection_percent, xover_percent, mutate_percent, elite_nitems):
	"""
		Function single_genetic
		Executes a genetic algorithm over a 2D array.

		Input variables
			- n_generations		: Integer. Number of generations
			- init_population	: 2D Numpy Array. Each row of this is a solution
			- has_xover			: Bool. Defines if we have to apply Crossover
			- xover_percent		: Float. Amount percent of solutions which will be crossed over
			- has_mutate		: Bool. Defines if Mutation is applied
			- mutate_percent	: Float. Amount percent of solutions which will be mutated
			- has_elite			: Bool. Defines if Elite is applied
			- elite_nitems		: Integer. Number of items which will be considered Elite

		Output variables
			- population 		: 2D Numpy Array. The best solution.
	"""
	population = init_population

	# computing sizes
	[num_actions, num_items_per_action] = population.shape

	select_nitems = np.ceil( selection_percent * (num_actions - elite_nitems) ).astype(int)
	xover_nitems  = np.ceil( xover_percent * select_nitems).astype(int)
	mutate_nitems = np.ceil( mutate_percent * (select_nitems - xover_nitems) ).astype(int)

	print("\n*** GENERATION 0 ***")
	# Evaluating initial population
	rewards_arr = run_bipedal(init_population)
	print("Generation 0 :: Best reward: {:+f}".format(rewards_arr[np.argmax(rewards_arr)]))

	# Running through generations
	for generation in xrange(1,n_generations):

		print("\n*** GENERATION {:d}\n".format(generation))

		# Elite
		elite_items   = np.zeros([elite_nitems,num_items_per_action]);
		elite_rewards = np.zeros([elite_nitems])

		# filling 'elite_items' ant 'elite_rewards'
		for elite_count in xrange(0,elite_nitems):
			elite_item_idx = np.argmax(rewards_arr)

			# filling arrays
			elite_items[elite_count,:] = population[elite_item_idx, :]
			elite_rewards[elite_count] = rewards_arr[elite_item_idx]

			# removing from originals
			population  = np.delete(population, elite_item_idx, 0)
			rewards_arr = np.delete(rewards_arr, elite_item_idx, 0)

		# Selecting population to cross over and mutation
		selected_idxs = ruleta(rewards_arr, select_nitems)

		selected_items   = population[selected_idxs.astype(int),:]
		selected_rewards = rewards_arr[selected_idxs.astype(int) ]

		# Taking out selected items from main arrays
		population  = np.delete(population, [selected_idxs.astype(int)] , 0)
		rewards_arr = np.delete(rewards_arr, [selected_idxs.astype(int)], 0)

		# Shuffle selected items
		# np.random.shuffle(selected_items)

		# Crossover
		xover_items   = np.zeros([xover_nitems, num_items_per_action])
		xover_rewards = np.zeros([xover_nitems])

		for xover_idx in xrange(0, ((xover_nitems/2)-1), 2):
			fst_elem = selected_items[xover_idx, :]
			snd_elem = selected_items[(xover_idx+1), :]

			[ fst_elem_cross, snd_elem_cross ] = do_xover(fst_elem, snd_elem)

			xover_items[xover_idx    , :] = fst_elem_cross
			xover_items[(xover_idx+1), :] = snd_elem_cross
			xover_rewards[xover_idx  ] = None # Every new action ...
			xover_rewards[xover_idx+1] = None # 	... isn't evaluated yet

		# Taking out used elements from selected{ _items, _rewards }
		selected_items   = np.delete(selected_items  , np.s_[0:xover_nitems], 0)
		selected_rewards = np.delete(selected_rewards, np.s_[0:xover_nitems], 0)

		# Shuffle selected items
		# np.random.shuffle(selected_items)

		# Mutation
		mutated_items   = np.zeros([mutate_nitems, num_items_per_action])
		mutated_rewards = np.zeros([mutate_nitems])

		for mutate_idx in xrange(0, mutate_nitems):
			mutated = do_mutation(selected_items[mutate_idx, :])

			mutated_items[mutate_idx,:] = mutated
			mutated_rewards[mutate_idx] = None

			# Taking out used elements from selected
			selected_items   = np.delete(selected_items  , [mutate_idx], 0)
			selected_rewards = np.delete(selected_rewards, [mutate_idx], 0)

		# Merging/Concatenating/Appending everything
		population  = np.concatenate((population, elite_items, selected_items, xover_items, mutated_items), axis=0)
		rewards_arr = np.concatenate((rewards_arr, elite_rewards, selected_rewards, xover_rewards, mutated_rewards), axis=0)

		# Shuffling population (I don't know if it's correct)
		# np.random.shuffle(population)

		# Evaluating new population
		rewards_arr = run_bipedal(population)
		print("** Generation {:d} Best reward: {:+f}\n\n".format(generation,rewards_arr[np.argmax(rewards_arr)]))

	# End 'for' n_generations

	return rewards_arr

def single_genetic_multi_sol(n_generations, init_population, selection_percent, xover_percent, mutate_percent, elite_nsol):
	"""
		Function single_genetic_multi_sol
	"""

	# Just to clarify
	population = init_population

	# computing sizes
	[num_solutions, num_actions, num_items_per_action] = population.shape

	select_nsol = np.ceil( selection_percent * (num_solutions - elite_nsol) ).astype(int)
	xover_nsol  = np.ceil( xover_percent * select_nsol).astype(int)
	mutate_nsol = np.ceil( mutate_percent * (select_nsol - xover_nsol) ).astype(int)

	# Initial population evaluation
	print("\n*** GENERATION 0 ***")
	# Rewards matrix
	rewards_arr = run_bipedal(init_population)

	rewards_arr_totals = np.sum(rewards_arr, axis=0)
	print( "Generation 0 :: Best reward: {:+f}".format( rewards_arr_totals[ np.argmax( rewards_arr_totals ) ] ) )

	# Running through generations
	for generation in xrange(1,n_generations):
		print("\n*** GENERATION {:d}\n".format(generation))

		# Elite
		elite_items   = np.zeros([elite_nsol, num_actions, num_items_per_action]);
		elite_rewards = np.zeros([num_actions, elite_nsol])
		# filling 'elite_items' and 'elite_rewards'
		for elite_count in xrange(0,elite_nsol):
			elite_item_idx = np.argmax( rewards_arr_totals )

			# filling arrays # __To do__
			elite_items[elite_count,:,:] = population[elite_item_idx,:,:]
			elite_rewards[:,elite_count] = rewards_arr[:,elite_item_idx]

			# removing from originals
			population  = np.delete(population , elite_item_idx, 0)
			rewards_arr = np.delete(rewards_arr, elite_item_idx, 1)

		# Recalculating
		rewards_arr_totals = np.sum(rewards_arr, axis=0)

		# Selecting population to cross over and mutation
		selected_idxs = ruleta(rewards_arr_totals, select_nsol)

		selected_items   = population[selected_idxs.astype(int),:,:]
		selected_rewards = rewards_arr[:,selected_idxs.astype(int)]

		# Taking out selected items from main arrays
		population  = np.delete(population, [selected_idxs.astype(int)] , 0)
		rewards_arr = np.delete(rewards_arr, [selected_idxs.astype(int)], 1)

		# Shuffle selected items
		# np.random.shuffle(selected_items)

		# Crossover
		xover_items   = np.zeros([xover_nsol, num_actions, num_items_per_action])
		xover_rewards = np.zeros([num_actions, xover_nsol])
		xover_rewards[:] = 0   # Every crossed over element hasn't reward

		print(xover_nsol)

		for xover_idx in xrange(0, ((xover_nsol/2)-1), 2):
			fst_elem 	 = selected_items[xover_idx,:,:]
			fst_elem_rew = selected_rewards[:,xover_idx]
			snd_elem 	 = selected_items[(xover_idx+1),:,:]
			snd_elem_rew = selected_rewards[:,xover_idx]

			print("BEFORE XOVER")
			print(fst_elem.shape)
			print(snd_elem.shape)

			[ fst_elem_cross, snd_elem_cross ] = do_xover(fst_elem, fst_elem_rew, snd_elem, snd_elem_rew)

			print("AFTER XOVER")
			print(fst_elem_cross.shape)
			print(snd_elem_cross.shape)

			sys.exit(1)

			xover_items[xover_idx    ,:,:] = fst_elem_cross
			xover_items[(xover_idx+1),:,:] = snd_elem_cross

		# Taking out used elements from selected{ _items, _rewards }
		selected_items   = np.delete(selected_items  , np.s_[0:xover_nsol], 0)
		selected_rewards = np.delete(selected_rewards, np.s_[0:xover_nsol], 0)

		# Shuffle selected items
		# np.random.shuffle(selected_items)

		# Mutation
		mutated_items   = np.zeros([mutate_nsol, num_actions, num_items_per_action])
		mutated_rewards = np.zeros([mutate_nsol])

		for mutate_idx in xrange(0, mutate_nsol):
			mutated = do_mutation(selected_items[mutate_idx,:,:])

			mutated_items[mutate_idx,:,:] = mutated
			mutated_rewards[mutate_idx] = None

			# Taking out used elements from selected
			selected_items   = np.delete(selected_items  , [mutate_idx], 0)
			selected_rewards = np.delete(selected_rewards, [mutate_idx], 0)

		# Merging/Concatenating/Appending everything
		population  = np.concatenate((population, elite_items, selected_items, xover_items, mutated_items), axis=0)
		rewards_arr = np.concatenate((rewards_arr, elite_rewards, selected_rewards, xover_rewards, mutated_rewards), axis=0)

		# Evaluating new population
		rewards_arr = run_bipedal(population)
		print("** Generation {:d} Best reward: {:+f}\n\n".format(generation,rewards_arr[np.argmax(rewards_arr)]))

	# End 'for' n_generations

	return rewards_arr