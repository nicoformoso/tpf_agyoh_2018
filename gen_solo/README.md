# Esta implementación sólo utiliza un algoritmo genético
## _DEPRECADO_

Este código no es el utilizado como solución del problema planteado, pero de él pueden obtenerse ejemplos para la implementación de un algoritmo genético con múltiples soluciones y con una sola solución. La insercion de soluciones múltiples está incompleta.
Su desarrollo llega hasta el commit [`394f49c`](https://bitbucket.org/nicoformoso/tpf_agyoh_2018/commits/394f49c626a851077e723e42ecde24bdeb93e2fa).