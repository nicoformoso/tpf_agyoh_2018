#
# This is the main executable script for running the genetic algorithm
# to reach the sequence of 'action' arrays to make the BipedalWalker-v2
# walk
#
import numpy as np
from set_initial_population import initial_population as set_init_pop

# from genetic_algorithms import single_genetic_single_sol as run_genetic
from genetic_algorithms import single_genetic_multi_sol as  run_genetic

#
# Variable definition
#

# Number of generations
num_generations = 100

# Number of solutions
num_solutions = 16

# Those variables below, define a solution matrix size: 1600x4
# num_actions = 1600 # ORIGINAL
num_actions = 100
num_items_per_action = 4

# These variables below, define if crossover, mutation and elite
# operations should be done.
# Selection percent is computed over whole population size.
selection_percent = 0.5
# Crossover percent is computed over selection size.
xover_percent  = 0.9
# Mutation percent is computed over selection without crossover.
mutate_percent = 0.05
# Elite is the number of best items within the whole new generation.
elite_nitems = 1

#
# Initial population generation
#
initial_rand_population = set_init_pop(num_actions, num_items_per_action, num_solutions);
print("\n*** Poblacion inicial aleatoria generada.\n");
print("*** Generando {:d} soluciones de {:d}x{:d}.\n".format(num_solutions,num_actions,num_items_per_action))

#
# Genetic Algorithm Execution
#
action_solution = run_genetic(
	num_generations, 
	initial_rand_population, 
	selection_percent, 
	xover_percent, 
	mutate_percent, elite_nitems)

#
# Execute found solution (not implemented yet)
#

#
# Generate graphics (not implemented yet)
#