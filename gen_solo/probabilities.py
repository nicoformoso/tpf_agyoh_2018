import numpy as np

def calc_probabilities( values ):
	"""
		Computes probabilities of each value within vector
		'values', and returns an array with it
	"""
	epsilon = 0.1 # This is the minimum probability
	values_prob = values - values[np.argmin(values)] + epsilon

	if np.any(values_prob): # True, if any element is other than zero
							# False, otherwise
		probability = values_prob / np.sum(values_prob)
	else:
		probability = np.ones( np.size(values_prob) ) / np.size(values_prob)

	return probability
