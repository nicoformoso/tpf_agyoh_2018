import numpy as np

def crossover_single( element1, element2 ):

	num_items = element1.size

	if ( num_items != element2.size ):
		print("ERROR :: crossover_single :: Input arguments size doesn't match.")
		sys.exit(1)

	# I should check if cut_point is integer
	cut_point = num_items/2

	cross_element1 = element1
	cross_element1[cut_point:num_items] = element2[0:cut_point]
	cross_element2 = element2
	cross_element2[0:cut_point] = cross_element1[cut_point:num_items]

	return [cross_element1, cross_element2]

def crossover_multi( element1, element1_rew, element2, element2_rew ):

	# Shape is the same to both matrixes
	[num_actions, num_elements] = element1.shape

	[cut_point1, length1] = get_xover_cutpoint(element1_rew)
	[cut_point2, length2] = get_xover_cutpoint(element2_rew)

	# Looking for the shortest length
	if length1 < length2:
		length = length1
	else:
		length = length2

	if length > (num_actions/2):
		length = (num_actions/2)

	# element1 parts
	element1_init  = element1[0,cut_point1,:]
	element1_xover = element1[(cut_point1+1),(cut_point1+length),:]
	element1_last  = element1[(cut_point1+length+1),num_actions,:]

	# element2 parts
	element2_init  = element2[0,cut_point2,:]
	element2_xover = element2[(cut_point2+1),(cut_point2+length),:]
	element2_last  = element2[(cut_point2+length+1),num_actions,:]

	cross_element1 = np.zeros((num_actions,num_elements))
	cross_element2 = np.zeros((num_actions,num_elements))

	cross_element1[0,cut_point1,:] = element1_init
	cross_element1[(cut_point1+1),(cut_point1+length),:] = element2_xover
	cross_element1[(cut_point1+length+1),num_actions,:]  = element1_last

	cross_element2[0,cut_point2,:] = element2_init
	cross_element2[(cut_point2+1),(cut_point2+length),:] = element1_xover
	cross_element2[(cut_point2+length+1),num_actions,:]  = element2_last

	return [cross_element1, cross_element2]

def get_xover_cutpoint( rewards ):
	return [cut_point, length]