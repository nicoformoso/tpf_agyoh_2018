from datetime import datetime
import time

import random
import numpy as np

def initial_population( num_rows, num_cols, num_matrix = 1, seed = None ):
	"""
		This function returns a numpy array of num_rows by num_cols size,
		whose values are random between -1 and 1.
	"""

	# Each action value is within range [-1,1]
	min_val = -1
	max_val =  1

	# If seed is not provided, we generate one
	if not seed:
		dt = datetime.today()
		seed = time.mktime(dt.timetuple())
	random.seed(seed)

	solution   = np.empty([num_rows, num_cols])
	population = np.empty([num_matrix, num_rows, num_cols])

	for idx_matrix in xrange(0,num_matrix):

		for idx_row in xrange(0,num_rows):
			for idx_col in xrange(0,num_cols):
				solution[idx_row, idx_col] = random.uniform(min_val, max_val);
		population[idx_matrix,:,:] = solution

	if num_matrix == 1:
		return solution
	else:
		return population
