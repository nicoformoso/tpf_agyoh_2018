# Algoritmo Genético para el caminante bípedo de la plataforma de OpenAI.org

* **Carrera:** Maestría y Especialización en Ingeniería en sistemas de Información
* **Materia:** Algoritmos Genéticos y Optimización Heurística
* **Cohorte:** 2017
* **Integrantes:**
	1. Nicolás Federico Formoso Requena
	2. Sergio Pérez Albert

### Estructura de directorios

Archivo / Directorio 	|	Descripción
--------------------	|	-----------
gen_nn					|	Directorio donde se desarrolla la solución planteada para el caso
gen_solo				|	Directorio que contiene una solución _deprecada_ (puede tomarse sus códigos como ejemplo)
notes.txt 				|	Notas sobre particularidades del Caminante Bípedo 
test.py 				| 	Pruebas de funcionamiento del GYM de OpenAI
.gitignore 				|	Archivos / Directorios a ignorar por GIT
README.md 				| 	Este archivo

---

## Descripción
El presente repositorio contiene el código fuente en Python desarrollado para el trabajo práctico final de la materia: "Algoritmos Genéticos y Optimización Heurística"

Se trata de la implementación de un algoritmo genético utilizado para enseñar a caminar a _BipedalWalker-v2_, un robot de la plataforma de OpenAI.org.
Para mayor información sobre el mismo: [https://gym.openai.com/envs/BipedalWalker-v2/](https://gym.openai.com/envs/BipedalWalker-v2/)

### Características del entorno de trabajo

* **Sistema Operativo:** GNU/Linux Ubuntu 18.04 LTS
* **Python2 version:** 2.7.15rc1
* **Python3 version:** 3.6.5

---

## Proceso de instalación (GNU/Linux)

1. Instalación de dependencias: pip, swig, cmake, zlib1g-dev.
```
sudo apt install python-pip python3-pip swig cmake zlib1g-dev
```
Se omite la referencia a errores que surgieron al momento de intentar compilar librerías python, a causa de la falta de los paquetes mencionados, y a raíz de lo cual se los indica como dependencias.

2. Instalación de la librería [`gym`](https://github.com/openai/gym) de [OpenAI](https://openai.com/), mediante compilación del código fuente.
```
git clone https://github.com/openai/gym.git
cd gym
pip install -e '.[all]'
```
Esto garantiza la instalación de la librería `gym` y todas sus dependencias. 

### Problemas enfrentados

#### Inconvenientes entre la versión instalada de Swig y Box2D 
Al intentar realizar la instalación de la librería `gym` a través de PIP, según se indica en el propio sitio de OpenAI: `pip install gym`; cuando se quiere ejecutar _BipedalWalker-v2_, se requiere la instalación por separado de la dependencia `Box2D`: `pip install Box2D`.
Aparentemente, existe un problema con la última versión de Swig y la librería Box2D que puede solucionarse siguiendo el proceso de instalación mencionado más arriba.

Este inconveniente se encuentra documentado en el link [https://github.com/openai/gym/issues/100](https://github.com/openai/gym/issues/100)

---

## Pruebas

Para probar si la librería gym se encuentra correctamente instalada, puede ejecutarse el archivo de prueba que se adjunta `test.py`.

---

## Documentación

### OpenAI
Para la utilización de la librería, puede seguirse como referencia la documentación provista por OpenAI: [https://gym.openai.com/docs/](https://gym.openai.com/docs/).